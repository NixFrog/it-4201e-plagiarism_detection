solution "TD_algo"
	configurations ""
	project "td1"
		kind "ConsoleApp"
		language "C++"
		files({ "src/*.hpp", "src/*.cpp" })
		--includedirs({"/", "includes"})

		flags({"Symbols", "ExtraWarnings"})
		links({})
		--libdirs({"Driver/"})

		buildoptions({"-Wextra", "-std=c++14"})
		linkoptions({})

