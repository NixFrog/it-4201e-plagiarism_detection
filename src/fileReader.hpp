#ifndef __FILE_READER__
#define __FILE_READER__

#include <iostream>
#include <fstream>

class FileReader
{
public:
    FileReader(const std::string& filePath);
    std::string getString() const;

private:
    std::string m_FilePath;
    std::string m_Content;
};

#endif // __FILE_READER__
