#ifdef __DISTANCE_ALGORITHM_HPP__

template<typename T>
void computeLevensteinDistance(std::string leftPage, std::string rightPage, std::vector<std::vector<T>>& vec, size_t rows, size_t cols)
{
    vec.at(0).at(0) = 0;

    for(size_t i = 1; i < cols; i++){
        vec.at(0).at(i) = vec.at(0).at(i-1) + ins(rightPage[i-1]);
    }

    for(size_t j = 1; j < rows; j++){
        vec.at(j).at(0) = vec.at(j-1).at(0) + del(leftPage[j-1]);
    }

    for(size_t i = 1; i < rows; i++){
        for(size_t j = 1; j <cols; j++){
            vec.at(i).at(j) = std::min(
                                std::min(
                                    vec.at(i).at(j-1)   + ins(rightPage[j-1]),
                                    vec.at(i-1).at(j-1) + sub(leftPage[i-1], rightPage[j-1])
                                ),  vec.at(i-1).at(j)   + del(leftPage[i-1])
                            );
        }
    }
}

template<typename T>
void backtraceToInsert(std::string& leftPage, std::string& rightPage, const std::vector<std::vector<T>>& vec, size_t rows, size_t cols)
{
    size_t min, i, j;
    i = rows - 1;
    j = cols - 1;

    while(i != 0 && j != 0){
        min = std::min(
                std::min(
                    vec.at(i-1).at(j-1)
                    , vec.at(i).at(j-1)
                ), vec.at(i-1).at(j)
        );

        if(min == vec.at(i).at(j-1)){
            leftPage.insert(i-1, 1, ' ');
            j--;
        }
        else if(min == vec.at(i-1).at(j)){
            rightPage.insert(j-1, 1, ' ');
            i--;
        }
        else{
            i--;
            j--;
        }
    }
}

template<typename T>
void printDistanceVector(std::string leftPage, std::string rightPage, const std::vector<std::vector<T>>& vec, size_t rows, size_t cols)
{
    size_t distance = vec.at(rows - 1).at(cols - 1);
    float percentage = 100 * distance / std::max(rows, cols);

    std::cout<<"Distance between texts:    "<< distance<<std::endl;
    std::cout<<"Length of the first text:  "<<rows<<std::endl;
    std::cout<<"Length of the second text: "<<cols<<std::endl;
    std::cout<<"Percentage of difference:  "<<percentage << "%" <<std::endl;
}

template<typename T>
void displayDistanceInformation(const std::vector<std::vector<T>>& vec, size_t rows, size_t cols)
{
    size_t distance = vec.at(rows - 1).at(cols - 1);
    float percentage = 100 * distance / std::max(rows, cols);

    std::cout<<"Distance between texts:    "<< distance<<std::endl;
    std::cout<<"Length of the first text:  "<<rows<<std::endl;
    std::cout<<"Length of the second text: "<<cols<<std::endl;
    std::cout<<"Percentage of difference:  "<<percentage << "%" <<std::endl;
}

#endif // __DISTANCE_ALGORITHM_HPP__
