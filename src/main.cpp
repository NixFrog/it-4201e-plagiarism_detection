#include <iostream>
#include <string>
#include <vector>

#include "fileReader.hpp"
#include "text_comparator.hpp"

int main(int argc, char const *argv[]) {
    if(argc != 3){
		std::cerr<<"Insufficient number of arguments. Usage: prog <FileName_1> <FileName_2>"<<std::endl;
		exit(EXIT_FAILURE);
	}

    FileReader leftFileReader(argv[1]);
    std::string leftString = leftFileReader.getString();
    FileReader rightFileReader(argv[2]);
    std::string rightString = rightFileReader.getString();

    TextComparator cmp(leftString, rightString);
    cmp.computeDistance();
    cmp.alignTexts();
    std::cout<<cmp<<std::endl;

    return 0;
}
