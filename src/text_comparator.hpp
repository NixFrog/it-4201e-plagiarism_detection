#ifndef __TEXT_COMPARATOR_HPP__
#define __TEXT_COMPARATOR_HPP__

// #define DEBUG

#include <string>
#include <iostream>
#include <vector>

#include <sys/ioctl.h>
#include <unistd.h>

#include "distance_algorithm.hpp"

class TextComparator
{
public:
    TextComparator(const std::string& leftPage, const std::string& rightPage);
    void computeDistance();
    void alignTexts();
    size_t getDistance() const;

    friend std::ostream& operator<<(std::ostream& os, const TextComparator& comparator);

private:
    std::string leftPage_;
    std::string rightPage_;
    std::vector<std::vector<size_t>> distanceVector_;
    bool distanceIsComputed_;
    size_t rows_, cols_;
};

inline std::ostream& operator<<(std::ostream& os, const TextComparator& textComparator)
{
    struct winsize termSize;
    size_t lineSize;
    std::string leftPageLine = "", rightPageLine = "", horizontalSplitter;
    bool leftIsWaiting = false, rightIsWaiting = false;

    ioctl(STDOUT_FILENO,TIOCGWINSZ,&termSize);
    lineSize = (termSize.ws_col - 7)/2;

    size_t lIndex = 0, rIndex = 0;

    DistanceAlgorithm::displayDistanceInformation(textComparator.distanceVector_, textComparator.rows_, textComparator.cols_);

    horizontalSplitter += "+-";
    horizontalSplitter.append(lineSize, '-');
    horizontalSplitter += "-+-";
    horizontalSplitter.append(lineSize, '-');
    horizontalSplitter += "-+";

    os << horizontalSplitter << std::endl;

    while(textComparator.leftPage_[lIndex] != '\0'
            || textComparator.rightPage_[rIndex] != '\0'
        )
    {
        if(textComparator.leftPage_[lIndex] == '\n' || textComparator.leftPage_[lIndex] == '\0'){
            leftIsWaiting = true;
        }
        if(textComparator.rightPage_[rIndex] == '\n' || textComparator.rightPage_[rIndex] == '\0'){
            rightIsWaiting = true;
        }

        if(leftIsWaiting && rightIsWaiting){
            if(leftPageLine.size() - lineSize != 0){
                leftPageLine  += " ";
                rightPageLine += " ";
            }
            else{
                os << "| " << leftPageLine << " | " << rightPageLine << " |" << std::endl;
                os << horizontalSplitter << std::endl;
                leftIsWaiting  = false;
                rightIsWaiting = false;
                leftPageLine   = "";
                rightPageLine  = "";
                lIndex++;
                rIndex++;
            }
        }
        else{
            if(leftPageLine.size() - lineSize != 0){
                if(!leftIsWaiting){
                    leftPageLine += textComparator.leftPage_[lIndex];
                    lIndex++;
                }
                else{
                    leftPageLine += " ";
                }
                if(!rightIsWaiting){
                    rightPageLine += textComparator.rightPage_[rIndex];
                    rIndex++;
                }
                else{
                    rightPageLine += " ";
                }
            }
            else{
                os << "| " << leftPageLine << " | " << rightPageLine << " |" << std::endl;
                leftPageLine = "";
                rightPageLine = "";
            }
        }
    }

    while(leftPageLine.size() - lineSize != 0){
        leftPageLine  += " ";
        rightPageLine += " ";
    }

    os << "| " << leftPageLine << " | " << rightPageLine << " |" << std::endl;
    os << horizontalSplitter << std::endl;

    return os;
}

#endif // __TEXT_COMPARATOR_HPP__
