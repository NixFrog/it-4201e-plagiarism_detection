#ifndef __DISTANCE_ALGORITHM_HPP__
#define __DISTANCE_ALGORITHM_HPP__

#include <iostream>
#include <algorithm>
#include <vector>

namespace DistanceAlgorithm
{
    template<typename T>
    void computeLevensteinDistance(std::string leftPage, std::string rightPage, std::vector<std::vector<T>>& vec, size_t rows, size_t cols);

    template<typename T>
    void backtraceToInsert(std::string& leftPage, std::string& rightPage, const std::vector<std::vector<T>>& vec, size_t rows, size_t cols);

    template<typename T>
    void printDistanceVector(std::string leftPage, std::string rightPage, const std::vector<std::vector<T>>& vec, size_t rows, size_t cols);

    template<typename T>
    void displayDistanceInformation(const std::vector<std::vector<T>>& vec, size_t rows, size_t cols);

    inline size_t del(char a){return a == '\0' ? 0 : 1;}
    inline size_t ins(char a){return a == '\0' ? 0 : 1;}
    inline size_t sub(char a, char b){return a == b ? 0 : 1;}

    inline size_t del(std::string a){return a.size();}
    inline size_t ins(std::string a){return a.size();}
    inline size_t sub(std::string a, std::string b)
    {
        size_t rows = a.size()+1;
        size_t cols = b.size()+1;
        std::vector<std::vector<size_t>> vec(rows, std::vector<size_t>(cols));
        computeLevensteinDistance(a, b, vec, rows, cols);
        return vec.at(rows).at(cols);
    }

    #include "distance_algorithm.tpp"
}

#endif // __DISTANCEç_ALGORITHM_HPP__
