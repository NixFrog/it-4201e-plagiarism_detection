#include "fileReader.hpp"

FileReader::FileReader(const std::string& filePath)
    : m_FilePath(filePath)
{
    std::string tmp;
    std::ifstream os;
    os.open(filePath);

    if(!os.is_open()){
        std::cerr<<"Error opening file "<<filePath<<std::endl;
        exit(EXIT_FAILURE);
    }
    while(std::getline(os, tmp)){
        m_Content += tmp;
        m_Content += '\n';
    }
    m_Content += '\0';
    os.close();
}


std::string FileReader::getString() const
{
    return m_Content;
}
