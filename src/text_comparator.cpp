#include "text_comparator.hpp"

TextComparator::TextComparator(const std::string& leftPage, const std::string& rightPage)
    : leftPage_(leftPage)
    , rightPage_(rightPage)
    , distanceVector_(leftPage.size()+1, std::vector<size_t>(rightPage.size()+1))
{
    distanceIsComputed_ = false;
    rows_ = leftPage_.size()+1;
    cols_ = rightPage_.size()+1;
}

size_t TextComparator::getDistance() const
{
    if(!distanceIsComputed_){
        std::cerr<<"Distance required when not computed"<<std::endl;
    }
    return distanceVector_.at(rows_).at(cols_);
}

void TextComparator::computeDistance()
{
    distanceIsComputed_ = true;
    std::cout<<"Computing Distance..."<<std::endl;
    DistanceAlgorithm::computeLevensteinDistance(leftPage_, rightPage_, distanceVector_, rows_, cols_);
}

void TextComparator::alignTexts()
{
    if(!distanceIsComputed_){
        std::cerr<<"Distance required when not computed"<<std::endl;
        computeDistance();
    }

    #ifdef DEBUG
        DistanceAlgorithm::printDistanceVector(leftPage_, rightPage_, distanceVector_, rows_, cols_);
    #endif // DEBUG

    std::cout<<"Aligning texts..."<<std::endl;
    DistanceAlgorithm::backtraceToInsert(leftPage_, rightPage_, distanceVector_, rows_, cols_);
}
